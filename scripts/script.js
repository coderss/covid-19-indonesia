$(document).ready(function () {
  // Push down the element under navbar
  $("#iconMenu").on("click", function () {
    $("#pushContent").slideToggle();
  });

  $('a[href^="#"]').click(function () {
    var the_id = $(this).attr("href");
    if (the_id === "#") {
      return;
    }
    $("html, body").animate(
      {
        scrollTop: $(the_id).offset().top - 70,
      },
      800
    );
    return false;
  });

  $("#month-year").text(moment().format("MMMM YYYY"));

  // GET API DATA CORONA
  var dataCovidIndonesia;
  $.ajax({
    type: "GET",
    url: "https://apicovid19indonesia-v2.vercel.app/api/indonesia",
    dataType: "json",
    success: function (response) {
      $(".positif-case").text(formatNumber(response.positif));
      $(".treated-case").text(
        formatNumber(response.dirawat)
      );
      $(".recovered-case").text(formatNumber(response.sembuh));
      $(".death-case").text(formatNumber(response.meninggal));
      $(".date-last-update").text(
        moment(response.lastUpdate).format("D MMMM YYYY, H:mm")
      );
    },
    error: function (xhr, textStatus, errorThrown) {
      console.log("Something error when get data Covid-19");
    },
  });


  // Daterangepicker
  var start = moment().subtract(29, 'days');
  var end = moment();

  function cb(start, end) {
      $('#dateReportCovid19 span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
  }

  $('#dateReportCovid19').daterangepicker({
      startDate: start,
      endDate: end,
      minDate: "03/01/2020",
      maxDate: new Date(),
      ranges: {
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);

  cb(start, end);

  

  // Chart JS, Statistik COVID-19 Indonesia
  // ** This API not available to get data from the current date. For example, the current date is 6 February 2021, you must define 1 day before that date as "endDate"

  // Get Data Covid-19 Indonesia
  var endDate = new Date().toISOString();
  endDate = endDate.split("T")[0] + "T00:00:00Z";
  var startDate = new Date(new Date().setDate(new Date().getDate() - 28)).toISOString();
  startDate = startDate.split("T")[0] + "T00:00:00Z";
  var dataCovid;

  // Report Data Chart
  $.ajax({
    type: "GET",
    url: "https://api.covid19api.com/country/indonesia",
    data: { from: startDate, to: endDate },
    success: function (response) {
      dataCovid = response;
      var reportDataActive = [];
      var reportDataRecovered = [];
      var reportDataDeaths = [];

      // Get Report Data Active
      $.each(dataCovid, function (i, value) {
          reportDataActive.push(value.Active);
      });

      // Get Report Data Recovered
      $.each(dataCovid, function (i, value) {
          reportDataRecovered.push(value.Recovered);
      });

      // Get Report Data Deaths
      $.each(dataCovid, function (i, value) {
          reportDataDeaths.push(value.Deaths);
      });

      // Create Label
      var labels = getDates(startDate, endDate);

      var bodyWidth = $(window).width();
      if (bodyWidth <= 540) {
        $('#confirmedChart').remove();
        $('#dateReportCovid19').after(`<canvas id="confirmedChart" height="250"></canvas>`);
      }
      setTimeout(() => {
        generateChart(reportDataActive, reportDataRecovered, reportDataDeaths, labels)
      }, 100);
    },
    error: function(err) {
      $('#confirmedChart').hide();
      $('#dateReportCovid19').hide();
      $('.chart-wrapper').html(`
        <img src="styles/assets/images/info-image.svg" alt="Couch potato illustration" style="width: 50%; display:block; margin: 0 auto;">
        <h5 class="text-center">Maaf data sementara tidak dapat ditampilkan, coba lagi nanti.</h5>
      `)
    }
  });

  $('#dateReportCovid19').on('apply.daterangepicker', function(ev, picker) {
    // Add 1 days to date because result from toIsoString format minus 1 day
    var startDate = addDays(1)(picker.startDate.format('MM/DD/YYYY')).toISOString();
    startDate = startDate.split("T")[0] + "T00:00:00Z";

    if (picker.endDate.format('MM/DD/YYYY') == moment().format('MM/DD/YYYY')) {
      var endDate = new Date(picker.endDate.format('MM/DD/YYYY')).toISOString();
    }else {
      var endDate = addDays(1)(picker.endDate.format('MM/DD/YYYY')).toISOString();
    }

    endDate = endDate.split("T")[0] + "T00:00:00Z";

    // Report Data Chart
    $.ajax({
      type: "GET",
      url: "https://api.covid19api.com/country/indonesia",
      data: { from: startDate, to: endDate },
      success: function (response) {
        dataCovid = response;
        var reportDataActive = [];
        var reportDataRecovered = [];
        var reportDataDeaths = [];

        // Get Report Data Active
        $.each(dataCovid, function (i, value) {
            reportDataActive.push(value.Active);
        });

        // Get Report Data Recovered
        $.each(dataCovid, function (i, value) {
            reportDataRecovered.push(value.Recovered);
        });

        // Get Report Data Deaths
        $.each(dataCovid, function (i, value) {
            reportDataDeaths.push(value.Deaths);
        });

        // Create Label
        var labels = getDates(startDate, endDate);
        $('#confirmedChart').remove();
        var bodyWidth = $(window).width();
        if (bodyWidth <= 540) {
          $('#dateReportCovid19').after(`<canvas id="confirmedChart" height="250"></canvas>`);
        }else {
          $('#dateReportCovid19').after(`<canvas id="confirmedChart" height="120"></canvas>`);
        }

        generateChart(reportDataActive, reportDataRecovered, reportDataDeaths, labels)
      },
      error: function(err) {
        $('#confirmedChart').hide();
        $('#dateReportCovid19').hide();
        $('.chart-wrapper').html(`
          <h3 class="text-center">STATISTIK PENYEBARAN COVID-19 DI INDONESIA</h3>
          <img src="styles/assets/images/info-image.svg" alt="Couch potato illustration" style="width: 50%; display:block; margin: 0 auto;">
          <h5 class="text-center">Data sementara tidak dapat ditampilkan, coba lagi nanti.</h5>
        `)
      }
    });
  });

  function generateChart(reportDataActive, reportDataRecovered, reportDataDeaths, labels)
  {
    var ctx = document.getElementById("confirmedChart");
      var myChart = new Chart(ctx, {
        type: "line",
        data: {
          labels: labels,
          datasets: [
            {
              label: "Pasien COVID-19 dalam perawatan",
              data: reportDataActive,
              fill: false,
              borderColor: "#ff9900",
              borderWidth: 3,
            },
            {
              label: "Pasien COVID-19 sembuh",
              data: reportDataRecovered,
              fill: false,
              borderColor: "#13c554",
              borderWidth: 3,
            },
            {
              label: "Pasien COVID-19 meninggal",
              data: reportDataDeaths,
              fill: false,
              borderColor: "#e44450",
              borderWidth: 3,
            },
          ],
        },
        options: {
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true,
                },
              },
            ],
          },
          tooltips: {
            callbacks: {
              title: function (tooltipItem, data) {
                var title =
                  tooltipItem[0].label;

                return title;
              },
              label: function(tooltipItem, data) {
                return data.datasets[tooltipItem['datasetIndex']].label + " : " + formatNumber(tooltipItem.value);
              },
            },
          },
          responsive: true,
        },
      });
  }

  function formatNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  function getDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push( moment(currentDate).format('DD MMM') )
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
  }

  const addDays = days => date => {
    const result = new Date(date);
  
    result.setDate(result.getDate() + days);
  
    return result;
  };
});
